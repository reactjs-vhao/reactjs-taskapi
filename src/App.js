// import "./App.css";
import BTTaskManager from "./components/BTTaskManager";

function App() {
  return (
    <div className="App">
      <BTTaskManager />
    </div>
  );
}

export default App;
