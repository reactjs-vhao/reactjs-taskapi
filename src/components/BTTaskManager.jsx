import React, { useEffect, useState } from "react";
import cn from "classnames";
import { toast } from "react-toastify";
import { btTaskManagerService } from "../services/btTaskManagerService";

const BTTaskManager = () => {
  const [taskList, setTaskList] = useState();
  const [formData, setFormData] = useState();
  const fetchTaskList = async () => {
    try {
      const data = await btTaskManagerService.getTaskList();
      setTaskList(data.data);
    } catch (error) {
      console.log(error);
    }
  };
  const handleFormData = () => (ev) => {
    const { name, value } = ev.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };
  useEffect(() => {
    fetchTaskList();
  }, []);
  return (
    <div>
      <h1>BTTaskManager</h1>
      <div>
        <form
          onSubmit={async (ev) => {
            try {
              ev.preventDefault();
              await btTaskManagerService.createTask(formData);
              fetchTaskList();
              toast.success("Create task success!");
            } catch (error) {
              toast.error("Create task error!");
              console.log(error);
            }
          }}
        >
          <input
            type="text"
            name="taskName"
            className="form-control"
            onChange={handleFormData()}
          />
          <div>
            <button className="btn btn-outline-success my-3">
              Create Task
            </button>
          </div>
        </form>
        <table className="table">
          <thead>
            <tr>
              <th>Task Name</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {taskList?.map((task, index) => (
              <tr key={index}>
                <td>{task?.taskName}</td>
                <td>
                  <p
                    className={cn("badge", {
                      "bg-danger": !task?.status,
                      "bg-success": task?.status,
                    })}
                  >
                    {task?.status ? "Done" : "Inprocessing"}
                  </p>
                </td>
                <td>
                  {!task?.status && (
                    <button
                      className="btn btn-success"
                      onClick={async () => {
                        try {
                          await btTaskManagerService.updateStatusTask(
                            task.taskName
                          );
                          fetchTaskList();
                          toast.success("Update task success!");
                        } catch (error) {
                          toast.error("Update task error!");
                        }
                      }}
                    >
                      done
                    </button>
                  )}
                  <button
                    className="btn btn-danger ml-3"
                    onClick={async () => {
                      try {
                        await btTaskManagerService.deleteTask(task.taskName);
                        fetchTaskList();
                        toast.success("Delete task success!");
                      } catch (error) {
                        toast.error("Delete task error!");
                      }
                    }}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default BTTaskManager;
